<?php

namespace Drupal\social_graphql\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Datetime\DateFormatter;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Logger\LoggerChannelTrait;
use Drupal\Core\Messenger\MessengerTrait;
use Drupal\Core\TempStore\PrivateTempStoreFactory;
use Drupal\social_graphql\Services\Facebook\FacebookPostCollector;
use Drupal\social_graphql\Services\SocialGraphQlManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class FacebookSettingsForm.
 *
 * @package Drupal\social_graphql\Form
 */
class FacebookSettingsForm extends SocialGraphQlSettingsFormBase {

  use LoggerChannelTrait;
  use MessengerTrait;

  /**
   * The facebook post collector service.
   *
   * @var \Drupal\social_graphql\Services\Facebook\FacebookPostCollector
   */
  protected $facebookService;

  protected $dateFormatter;

  protected $privateTempStore;

  /**
   * Social network name.
   */
  const SOCIAL_NETWORK = 'facebook';

  /**
   * Constructs a new GenerateExampleForm object.
   *
   * {@inheritdoc}
   */
  public function __construct(ConfigFactoryInterface $config_factory, FacebookPostCollector $facebook_post_collector, SocialGraphQlManager $social_graphQl_manager, DateFormatter $date_formatter, PrivateTempStoreFactory $private_tempstore) {
    parent::__construct($config_factory, $facebook_post_collector, $social_graphQl_manager);
    $this->facebookService = $facebook_post_collector;
    $this->dateFormatter = $date_formatter;
    $this->privateTempStore = $private_tempstore;

  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('social_graphql.' . self::SOCIAL_NETWORK),
      $container->get('social_graphql.manager'),
      $container->get('date.formatter'),
      $container->get('tempstore.private')
    );
  }

  /**
   * {@inheritdoc}
   */
  public static function getSociaNetwork() {
    return self::SOCIAL_NETWORK;
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'social_graphql.facebook',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'facebook_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('social_graphql.facebook');

    $form['page_name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Default Facebook Page Name'),
      '#default_value' => $config->get('page_name'),
      '#description' => $this->t('eg. If your Facebook page URL is this http://www.facebook.com/YOUR_PAGE_NAME, <br />then you just need to add this YOUR_PAGE_NAME above.'),
      '#size' => 60,
      '#maxlength' => 100,
      '#required' => TRUE,
    ];
    $app_id = $this->getDefaultValue($form_state, $config, 'app_id');
    $form['app_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Default Facebook App ID'),
      '#default_value' => $app_id,
      '#size' => 60,
      '#maxlength' => 255,
      '#required' => TRUE,
    ];
    $secret_key = $this->getDefaultValue($form_state, $config, 'secret_key');
    $form['secret_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Default Facebook Secret Key'),
      '#default_value' => $secret_key,
      '#size' => 60,
      '#maxlength' => 255,
      '#required' => TRUE,
    ];
    $access_token = $this->getDefaultValue($form_state, $config, 'access_token');
    $form['token'] = [
      '#type' => 'container',
      '#tree' => FALSE,
      '#prefix' => '<div id="ajax-access-token">',
      '#suffix' => '</div>',
    ];
    $form['token']['access_token'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Default Facebook User Token'),
      '#default_value' => $access_token,
      '#size' => 60,
      '#maxlength' => 255,
      '#required' => TRUE,
    ];
    $form['token']['token_detail'] = [
      '#type' => 'details',
      '#title' => $this->t('Token infos'),
    ];
    if (!empty($access_token)) {
      try {
        $debug = $this->facebookService->debugToken($access_token);
        $debug['data']['access_token'] = $access_token;
        if (!empty($debug['data'])) {
          $liste = $this->themeList($debug['data']);
          $form['token']['token_detail']['token_info'] = ['#markup' => render($liste)];
        }
        $form['token']['token_long_live'] = [
          '#type' => 'button',
          '#value' => $this->t('Extended lifetime'),
          '#ajax' => [
            'callback' => [$this, 'ajaxTokenLongLive'],
            'wrapper' => 'ajax-access-token',
            'method' => 'replaceWith',
            'progress' => [
              'type' => 'throbber',
              'message' => NULL,
            ],
          ],
          '#executes_submit_callback' => FALSE,

        ];
      }
      catch (\Exception $e) {
        $this->messenger()->addError($e->getMessage());
      }
    }
    return parent::buildForm($form, $form_state);
  }

  /**
   * Callback ajax to request a long live token.
   *
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *
   * @return mixed
   */
  public function ajaxTokenLongLive(array &$form, FormStateInterface $form_state) {
    $long = $this->facebookService->longLiveToken($form_state->getValue('app_id'), $form_state->getValue('secret_key'), $form_state->getValue('access_token'));
    if (!empty($long->access_token)) {
      // Request me account to set never expire.
      if (!empty($long->expires_in)) {
        $this->facebookService->getMeAccounts($long->access_token);
      }
      $form_state->setRebuild(TRUE);

      $tempstore = $this->privateTempStore->get('social_graphql');
      $tempstore->set('no_expire_token', $long->access_token);
      $form_state->setValue('access_token', $long->access_token);
      $form = $this->buildForm($form, $form_state);
      $form['token']['access_token']['#value'] = $long->access_token;
      $form['token']['access_token']['#suffix'] = $this->t('Long live token has been request and replace but not saved.');
    }
    return $form['token'];
  }

  /**
   * Recursive theme list builder.
   *
   * @param array $debug
   *   The array of access token debugged.
   *
   * @return array
   *   Render array of definition list.
   */
  private function themeList(array $debug) {
    $items = [];
    foreach ($debug as $field_name => $value) {
      if (!is_array($value) && !is_object($value)) {
        if (!empty($value) && in_array($field_name, $this->getDateField(), TRUE)) {
          $value = $this->dateFormatter->format($value);
        }
        $items[] = [
          'dt' => ['value' => $field_name],
          'dd' => ['value' => $value],
        ];

      }
      elseif (is_array($value)) {
        $sub_list = $this->themeList($value);
        $items[] = [
          'dt' => ['value' => $field_name],
          'dd' => ['value' => render($sub_list)],
        ];
      }
    }
    return [
      '#theme' => 'social_graphql_description_list',
      '#items' => $items,
    ];
  }

  /**
   * Return field name which must to be convert into readable date.
   *
   * @return array
   *   Name of the field to convert.
   */
  private function getDateField() {
    return ['data_access_expires_at', 'expires_at'];
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $access_token = $form_state->getValue('access_token');
    if (!empty($access_token)) {
      try {
        $this->facebookService->setSocial($form_state->getValue('app_id'), $form_state->getValue('secret_key'), $access_token);
        $debug = $this->facebookService->debugToken($access_token);
      }
      catch (\Exception $e) {
        $this->logger('social_graphql')->warning($e->getMessage());
        $this->messenger()->addWarning($e->getMessage());
      }
    }
  }

  /**
   * Callback ajax for long live token.
   *
   * {@inheritdoc}
   */
  public function ajaxToken(array &$form, FormStateInterface $form_state) {
    return $form['access_token'];
  }

  /**
   * @todo implements FB connect.
   */
  public function generateToken(array &$form, FormStateInterface $form_state) {
    $access_token = $this->facebookService->getAccessToken($form_state->getValue('app_id'), $form_state->getValue('secret_key'));

    if (!empty($access_token->access_token)) {

      $config = $this->config('social_graphql.facebook');
      $config->set('access_token', $access_token->access_token);
      $config->save();
    }
    return $form['access_token'];
  }

  public function submitForm(array &$form, FormStateInterface $form_state) {
    $tempstore = $this->privateTempStore->get('social_graphql');
    $no_expire_token = $tempstore->get('no_expire_token');
    if (!empty($no_expire_token)) {
      $form_state->setValue('access_token', $no_expire_token);
      $tempstore->delete('no_expire_token');
    }
    parent::submitForm($form, $form_state);
  }

}
