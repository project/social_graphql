<?php

namespace Drupal\social_graphql\Form;

use Drupal\Core\Config\Config;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\social_graphql\Services\SocialGraphQlManager;
use Drupal\social_graphql\Services\SocialPostCollectorInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class SocialGraphQlSettingsFormBase.
 *
 * @package Drupal\social_graphql\Form
 */
abstract class SocialGraphQlSettingsFormBase extends ConfigFormBase implements SocialGraphQlSettingsFormInterface {

  /**
   * @var \Drupal\social_graphql\Services\SocialPostCollectorInterface
   */
  protected $postCollector;

  /**
   *
   * @var \Drupal\social_graphql\Services\SocialGraphQlManager
   */
  protected $socialGraphQlManager;

  /**
   * Constructs a new GenerateExampleForm object.
   *
   * {@inheritdoc}
   */
  public function __construct(ConfigFactoryInterface $config_factory, SocialPostCollectorInterface $post_collector, SocialGraphQlManager $social_graphql_manager) {
    parent::__construct($config_factory);
    $this->postCollector = $post_collector;
    $this->socialGraphQlManager = $social_graphql_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('social_graphql.' . static::getSociaNetwork()),
      $container->get('social_graphql.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config_name = $this->getEditableConfigNames();
    if (is_array($config_name)) {
      $config_name = reset($config_name);
    }
    $config = $this->config($config_name);

    $mapping = $this->socialGraphQlManager->getMapping(static::getSociaNetwork());
    $options = array_combine(array_keys($mapping), array_keys($mapping));

    $entities = $this->getDefaultValue($form_state, $config, 'entities', $options);
    asort($options);
    $form['entities'] = [
      '#type' => 'select',
      '#multiple' => TRUE,
      '#ajax' => [
        'callback' => [$this, 'ajaxFields'],
        'wrapper' => 'ajax-fields',
        'method' => 'replaceWith',
        'progress' => [
          'type' => 'throbber',
          'message' => NULL,
        ],
      ],
      '#default_value' => $entities,
      '#title' => $this->t('Entities'),
      '#description' => $this->t('Entities available inside graphQL'),
      '#options' => $options,
    ];

    if (!empty($entities)) {
      $form['fields_tabs'] = [
        '#type' => 'vertical_tabs',
        '#title' => $this->t('Fields'),
        '#prefix' => '<div id="ajax-fields">',
        '#suffix' => '</div>',
      ];
      foreach ($mapping as $entity_name => $entity) {
        if (!in_array($entity_name, $entities)) {
          unset($mapping[$entity_name]);
          continue;
        }
        foreach ($entity as $field_name => $return_type) {
          // Entity field is not selected.
          if (strpos($return_type, 'SocialGraphQl') !== FALSE && !in_array($field_name, $entities, TRUE)) {
            unset($mapping[$entity_name][$field_name]);
          }
        }
      }

      foreach ($entities as $entity_name) {
        $form['social_gql_' . $entity_name] = [
          '#type' => 'details',
          '#tree' => TRUE,
          '#group' => 'fields_tabs',
          '#title' => $this->t('@entity field', ['@entity' => $entity_name]),
        ];
        $form['social_gql_' . $entity_name]['entity_name'] = [
          '#type' => 'value',
          '#value' => $entity_name,
        ];

        $options = array_combine(array_keys($mapping[$entity_name]), array_keys($mapping[$entity_name]));
        asort($options);
        $form['social_gql_' . $entity_name]['fields'] = [
          '#type' => 'select',
          '#multiple' => TRUE,
          '#default_value' => $this->getDefaultField($form_state, $config, $options, $entity_name),
          '#title' => $this->t('Fields'),
          '#description' => $this->t('Fields available inside graphQL'),
          '#options' => $options,
        ];
      }
    }
    return parent::buildForm($form, $form_state);
  }

  /**
   * Callback for ajax mapping field.
   *
   * {@inheritdoc}
   */
  public function ajaxFields(array $form, FormStateInterface $form_state) {
    return $form['fields_tabs'];
  }

  /**
   * Return the default value of field, works with ajax mod.
   *
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state data.
   * @param \Drupal\Core\Config\Config $config
   * @param $key_value
   * @param string $default_value
   *
   * @return array|mixed|string|null
   */
  public function getDefaultValue(FormStateInterface $form_state, Config $config, $key_value, $default_value = '') {
    $value = $form_state->getValue($key_value);
    if (!empty($value)) {
      return $value;
    }
    $value = $config->get($key_value);

    return !empty($value) ? $value : $default_value;
  }

  /**
   * Get default fields value.
   *
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   * @param \Drupal\Core\Config\Config $config
   * @param array $options_fields
   * @param $entity_name
   *
   * @return array
   */
  private function getDefaultField(FormStateInterface $form_state, Config $config, array $options_fields, $entity_name) {
    $value = $form_state->getValue('social_gql_' . $entity_name);
    if (!empty($value['fields'])) {
      $fields = $value['fields'];
    }
    else {
      $value = $config->get('social_gql_' . $entity_name);
      $fields = !empty($value['fields']) ? $value['fields'] : array_keys($options_fields);
    }
    foreach ($fields as $key => $default_fields) {
      if (!in_array($default_fields, $options_fields, TRUE)) {
        unset($fields[$key]);
      }
    }
    return $fields;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config_name = $this->getEditableConfigNames();
    if (is_array($config_name)) {
      $config_name = reset($config_name);
    }
    // Get the mapping and build config with mapping and the user selection.
    $mapping = $this->socialGraphQlManager->getMapping(static::getSociaNetwork());
    $config = $this->config($config_name);
    foreach ($form_state->getValues() as $key => &$value) {
      // Rebuilds fields.
      if (strpos($key, 'social_gql_') === 0) {
        if (empty($value['entity_name']) || empty($mapping[$value['entity_name']])) {
          continue;
        }
        foreach ($value['fields'] as $field_key => $field) {
          $value['fields_map'][$field_key] = $mapping[$value['entity_name']][$field_key];
        }
        if (!empty($mapping[$value['entity_name']]['social_graphql_type'])) {
          $value['fields_map']['social_graphql_type'] = $mapping[$value['entity_name']]['social_graphql_type'];
        }
      }
      $config->set($key, $value);
    }
    $config->save();
    parent::submitForm($form, $form_state);
  }

}
