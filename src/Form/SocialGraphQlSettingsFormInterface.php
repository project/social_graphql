<?php

namespace Drupal\social_graphql\Form;

/**
 * Interface SocialGraphQlSettingsFormInterface.
 *
 * @package Drupal\social_graphql\Form
 */
interface SocialGraphQlSettingsFormInterface {

  /**
   * Return the name of social network.
   *
   * @return string
   *   The name of social network
   */
  public static function getSociaNetwork();

}
