<?php

namespace Drupal\social_graphql\Form;

use Drupal\Core\Form\FormStateInterface;

/**
 * Class TwitterSettingsForm.
 *
 * @package Drupal\social_graphql\Form
 */
class TwitterSettingsForm extends SocialGraphQlSettingsFormBase {

  protected $twitterService;

  const SOCIAL_NETWORK = 'twitter';

  /**
   * {@inheritdoc}
   */
  public static function getSociaNetwork() {
    return self::SOCIAL_NETWORK;
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'social_graphql.twitter',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'twitter_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('social_graphql.twitter');
    $form['consumer_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Default Twitter Consumer Key'),
      '#default_value' => $config->get('consumer_key'),
      '#size' => 60,
      '#maxlength' => 100,
      '#required' => TRUE,
    ];
    $form['consumer_secret'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Default Twitter Consumer Secret'),
      '#default_value' => $config->get('consumer_secret'),
      '#size' => 60,
      '#maxlength' => 100,
      '#required' => TRUE,
    ];
    $form['access_token'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Default Twitter Access Token'),
      '#default_value' => $config->get('access_token'),
      '#size' => 60,
      '#maxlength' => 100,
      '#required' => TRUE,
    ];
    $form['access_token_secret'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Default Twitter Access Token Secret'),
      '#default_value' => $config->get('access_token_secret'),
      '#size' => 60,
      '#maxlength' => 100,
      '#required' => TRUE,
    ];
    $form['tweets_count'] = [
      '#type' => 'number',
      '#title' => $this->t('Default Tweet Count'),
      '#default_value' => $config->get('tweets_count'),
      '#size' => 60,
      '#maxlength' => 100,
      '#min' => 1,
      '#max' => 200,
    ];
    return parent::buildForm($form, $form_state);
  }

}
