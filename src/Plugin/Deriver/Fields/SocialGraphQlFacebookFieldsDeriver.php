<?php

namespace Drupal\social_graphql\Plugin\Deriver\Fields;

/**
 * Class SocialGraphQlFacebookFieldsDeriver.
 *
 * @package Drupal\social_graphql\Plugin\Deriver\Fields
 */
class SocialGraphQlFacebookFieldsDeriver extends SocialGraphQlFieldsDeriverBase {

  /**
   * Social network name.
   */
  const SOCIAL_NETWORK = 'facebook';

  /**
   * {@inheritdoc}
   */
  public function getSociaNetwork() {
    return self::SOCIAL_NETWORK;
  }

}
