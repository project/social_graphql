<?php

namespace Drupal\social_graphql\Plugin\Deriver\Fields;

use Drupal\Component\Plugin\Derivative\DeriverBase;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Plugin\Discovery\ContainerDeriverInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\graphql\Utility\StringHelper;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class SocialGraphQlFieldsDeriverBase.
 *
 * @package Drupal\social_graphql\Plugin\Deriver\Fields
 */
abstract class SocialGraphQlFieldsDeriverBase extends DeriverBase implements SocialGraphQlFieldsDeriverInterface, ContainerDeriverInterface {

  use StringTranslationTrait;

  /**
   * The config factory instance.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * {@inheritdoc}
   */
  public function __construct(ConfigFactoryInterface $config_factory) {
    $this->configFactory = $config_factory;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, $basePluginId) {
    // Instantiates this form class.
    return new static(
    // Load the service required to construct this class.
      $container->get('config.factory')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getDerivativeDefinitions($base_plugin_definition) {
    $config = $this->configFactory->get('social_graphql.' . $this->getSociaNetwork());

    if (!empty($config)) {
      $mapping = [];
      $conf = $config->get();
      foreach ($conf as $key => $fields) {
        if (strpos($key, 'social_gql_') === FALSE) {
          continue;
        }
        $mapping[$fields['entity_name']] = $fields['fields_map'];
      }
      $this->generateDeriver($base_plugin_definition, $mapping);
    }
    else {
      throw new \Exception($this->t('No mapping found for social graphql'));
    }

    return parent::getDerivativeDefinitions($base_plugin_definition);
  }

  /**
   * {@inheritdoc}
   */
  public function generateDeriver($base_plugin_definition, array $mapping) {
    foreach ($mapping as $type => $social_entity) {
      if (empty($social_entity['social_graphql_type'])) {
        throw new \Exception($this->t('No social graphql type found in mapping for @type', ['@type' => $type]));
      }
      foreach ($social_entity as $field_name => $return_type) {
        if ($field_name == 'social_graphql_type') {
          continue;
        }
        $derived_id = 'social_graphql.' . $this->getSociaNetwork() . '.' . $type . ':' . $field_name;
        $this->derivatives[$derived_id] = [
          'name' => StringHelper::propCase($field_name),
          'type' => $return_type,
          'field_name' => $field_name,
          'social_gql_object' => $type,
          'parents' => [$social_entity['social_graphql_type']],
        ];
        $this->derivatives[$derived_id] = $this->derivatives[$derived_id] + $base_plugin_definition;
      }

    }
  }

}
