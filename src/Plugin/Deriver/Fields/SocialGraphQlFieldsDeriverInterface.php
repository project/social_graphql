<?php

namespace Drupal\social_graphql\Plugin\Deriver\Fields;

/**
 * Interface SocialGraphQlFieldsDeriverInterface.
 *
 * @package Drupal\social_graphql\Plugin\Deriver\Fields
 */
interface SocialGraphQlFieldsDeriverInterface {

  /**
   * Generate the deriver with the field mapping.
   *
   * @param array|\Drupal\Component\Plugin\Definition\PluginDefinitionInterface $basePluginDefinition
   *   The definition of the base plugin from which the derivative plugin
   *   is derived. It is maybe an entire object or just some array, depending
   *   on the discovery mechanism.
   * @param array $parents
   * @param $deriver_name
   *
   * @return mixed
   */
  public function generateDeriver($base_plugin_definition, array $mapping);


  /**
   * The name of social netwok.
   *
   * @return string
   */
  public function getSociaNetwork();

}