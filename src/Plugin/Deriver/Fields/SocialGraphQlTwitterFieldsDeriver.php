<?php

namespace Drupal\social_graphql\Plugin\Deriver\Fields;

/**
 * Class SocialGraphQlTwitterFieldsDeriver.
 *
 * @package Drupal\social_graphql\Plugin\Deriver\Fields
 */
class SocialGraphQlTwitterFieldsDeriver extends SocialGraphQlFieldsDeriverBase {

  const SOCIAL_NETWORK = 'twitter';

  /**
   * {@inheritdoc}
   */
  public function getSociaNetwork() {
    return self::SOCIAL_NETWORK;
  }

}
