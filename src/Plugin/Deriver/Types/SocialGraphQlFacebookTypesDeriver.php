<?php

namespace Drupal\social_graphql\Plugin\Deriver\Types;

/**
 * Class SocialGraphQlFacebookTypesDeriver.
 *
 * @package Drupal\social_graphql\Plugin\Deriver\Types
 */
class SocialGraphQlFacebookTypesDeriver extends SocialGraphQlTypesDeriver {

  const SOCIAL_NETWORK = 'facebook';

  /**
   * {@inheritdoc}
   */
  public function getSociaNetwork() {
    return self::SOCIAL_NETWORK;
  }

}
