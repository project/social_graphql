<?php

namespace Drupal\social_graphql\Plugin\Deriver\Types;

/**
 * Interface SocialGraphQlTwitterFieldsDeriverInterface.
 *
 * @package Drupal\social_graphql\Plugin\Deriver\Twitter\Fields
 */
class SocialGraphQlTwitterTypesDeriver extends SocialGraphQlTypesDeriver {

  const SOCIAL_NETWORK = 'twitter';

  /**
   * {@inheritdoc}
   */
  public function getSociaNetwork() {
    return self::SOCIAL_NETWORK;
  }

}
