<?php

namespace Drupal\social_graphql\Plugin\Deriver\Types;

use Drupal\Component\Plugin\Derivative\DeriverBase;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Logger\LoggerChannelTrait;
use Drupal\Core\Plugin\Discovery\ContainerDeriverInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class SocialGraphQlTypesDeriver
 *
 * @package Drupal\social_graphql\Plugin\Deriver\Types
 */
abstract class SocialGraphQlTypesDeriver extends DeriverBase implements SocialGraphQlTypesDeriverInterface, ContainerDeriverInterface {

  use StringTranslationTrait;

  use LoggerChannelTrait;
  /**
   * The config factory instance.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * {@inheritdoc}
   */
  public function __construct(ConfigFactoryInterface $config_factory) {
    $this->configFactory = $config_factory;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, $basePluginId) {
    // Instantiates this form class.
    return new static(
    // Load the service required to construct this class.
      $container->get('config.factory')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getDerivativeDefinitions($basePluginDefinition) {
    $config = $this->configFactory->get('social_graphql.' . $this->getSociaNetwork());

    if (!empty($config)) {
      $mapping = [];
      $conf = $config->get();
      foreach ($conf as $key => $fields) {
        if (strpos($key, 'social_gql_') === FALSE) {
          continue;
        }
        $mapping[$fields['entity_name']] = $fields['fields_map'];
      }
      if (empty($mapping)) {
        $this->getLogger('social_graphql')->critical($this->t('No mapping found for social graphql'));
      }
      foreach ($mapping as $type => $social_entity) {
        if (empty($social_entity['social_graphql_type'])) {
          continue;
        }
        $derivative = [
            'name' => $social_entity['social_graphql_type'],
            'description' => $this->t("The field of '@type' .", [
              '@type' => $type,
            ]),
            'social_gql_object' => $type,
          ] + $basePluginDefinition;

        $derived_id = 'social_graphql_' . $this->getSociaNetwork() . '_' . $type;
        $this->derivatives[$derived_id] = $derivative;
      }

    }
    else {
      $this->getLogger('social_graphql')->critical($this->t('No mapping found for social graphql'));
    }


    return parent::getDerivativeDefinitions($basePluginDefinition);
  }

}
