<?php

namespace Drupal\social_graphql\Plugin\Deriver\Types;

/**
 * Interface SocialGraphQlPostDeriverInterface.
 *
 * @package Drupal\social_graphql\Plugin\Deriver
 */
interface SocialGraphQlTypesDeriverInterface {

  /**
   * Return the name of social network.
   *
   * @return string
   *   The name of social network
   */
  public function getSociaNetwork();

}
