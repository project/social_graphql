<?php

namespace Drupal\social_graphql\Plugin\GraphQL\Fields\Facebook;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\graphql\GraphQL\Execution\ResolveContext;
use Drupal\graphql\Plugin\GraphQL\Fields\FieldPluginBase;
use Drupal\social_graphql\Services\Facebook\FacebookPostCollector;
use GraphQL\Type\Definition\ResolveInfo;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Fetch data and load into post.
 *
 * @GraphQLField(
 *   id = "social_graphql_facebook",
 *   secure = true,
 *   name = "socialGraphQlFacebook",
 *   type = "[SocialGraphQlFacebookPost]",
 *   arguments = {
 *     "num_post" = "Int!"
 *   }
 * )
 */
class SocialGraphQlFacebook extends FieldPluginBase implements ContainerFactoryPluginInterface {

  /**
   * The Facebook service.
   *
   * @var \Drupal\social_graphql\Services\Facebook\FacebookPostCollector
   */
  protected $facebookService;

  /**
   * Config factory.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $config;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $pluginId, $pluginDefinition) {
    return new static(
      $configuration,
      $pluginId,
      $pluginDefinition,
      $container->get('social_graphql.facebook'),
      $container->get('config.factory')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $pluginId, $pluginDefinition, FacebookPostCollector $social_graphql_facebook, ConfigFactoryInterface $config_factory) {
    parent::__construct($configuration, $pluginId, $pluginDefinition);
    $this->facebookService = $social_graphql_facebook;
    $this->config = $config_factory->get('social_graphql.facebook');
  }

  /**
   * {@inheritdoc}
   */
  public function resolveValues($value, array $args, ResolveContext $context, ResolveInfo $info) {
    $posts = $this->facebookService->getPosts($this->config->get('page_name'), $args['num_post']);
    // Empty posts.
    if (empty($posts)) {
      yield   [
        'type' => 'SocialGraphQlFacebookPost',
        'post' => '',
      ];
    }
    // Fetch all posts.
    foreach ($posts as $post) {
      yield  [
        'type' => 'SocialGraphQlFacebookPost',
        'post' => $post,
      ];
    }
  }

}
