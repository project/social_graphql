<?php

namespace Drupal\social_graphql\Plugin\GraphQL\Fields\Facebook;

use Drupal\graphql\GraphQL\Execution\ResolveContext;
use Drupal\graphql\Plugin\GraphQL\Fields\FieldPluginBase;
use GraphQL\Type\Definition\ResolveInfo;

/**
 * Facebook graphql fields.
 *
 * @GraphQLField(
 *   id = "social_graphql_facebook_fields",
 *   secure = true,
 *   deriver =
 *   "Drupal\social_graphql\Plugin\Deriver\Fields\SocialGraphQlFacebookFieldsDeriver"
 * )
 */
class SocialGraphQlFacebookFields extends FieldPluginBase {

  /**
   * {@inheritdoc}
   */
  public function resolveValues($value, array $args, ResolveContext $context, ResolveInfo $info) {

    $plugin_definition = $this->getPluginDefinition();
    if (empty($plugin_definition['field_name']) || empty($plugin_definition['social_gql_object']) || empty($value[$plugin_definition['social_gql_object']])) {
      return '';
    }
    // Get the return graphql type.
    if (!empty($info->returnType->name)) {
      $type_name = $info->returnType->name;
    }
    elseif (!empty($info->returnType->ofType->name)) {
      $type_name = $info->returnType->ofType->name;
    }
    if (empty($type_name)) {
      return '';
    }
    $data = $value[$plugin_definition['social_gql_object']];
    $field_name = $plugin_definition['field_name'];
    $object = '';
    if (!empty($data[$field_name])) {
      $object = $data[$field_name];
    }
    elseif (!empty($data['data'][$field_name])) {
      $object = $data['data'][$field_name];
    }

    // Object Facebook.
    if (strpos($type_name, 'SocialGraphQlFacebook') !== FALSE) {

      if (is_array($object) && !empty($object['data'])) {
        foreach ($object['data'] as $obj) {
          yield [
            'type' => $type_name,
            $field_name => $obj,
          ];
        }
      }
      else {
        yield [
          'type' => $type_name,
          $field_name => $object,
        ];
      }
    }
    else {
      if (is_array($object)) {
        foreach ($object as $obj) {
          yield $obj;
        }
      }
      else {
        yield $object;
      }
    }
  }

}
