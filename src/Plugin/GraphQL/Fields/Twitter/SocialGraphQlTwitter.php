<?php

namespace Drupal\social_graphql\Plugin\GraphQL\Fields\Twitter;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\graphql\GraphQL\Execution\ResolveContext;
use Drupal\graphql\Plugin\GraphQL\Fields\FieldPluginBase;
use Drupal\social_graphql\Services\Twitter\TwitterPostCollector;
use GraphQL\Error\Error;
use GraphQL\Type\Definition\ResolveInfo;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Fetch data and load into post.
 *
 * @GraphQLField(
 *   id = "social_graphql_twitter",
 *   secure = true,
 *   type = "[SocialGraphQlTwitterPost]",
 *   name = "socialGraphQlTwitter",
 *   arguments = {
 *     "user_id" = "String",
 *     "count" = "Int",
 *     "screen_name" = "String",
 *     "exclude_replies" = "Boolean",
 *     "include_rts" = "Boolean",
 *     "tweet_mode" = {
 *       "type" = "String",
 *       "default" = "extended"
 *     },
 *     "include_entities" = {
 *       "type" = "Boolean",
 *       "default" = TRUE
 *     },
 *   }
 * )
 */
class SocialGraphQlTwitter extends FieldPluginBase implements ContainerFactoryPluginInterface {

  /**
   * The Twitter service.
   *
   * @var \Drupal\social_graphql\Services\TwitterPostCollector
   */
  protected $twitterService;

  /**
   * Config factory.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $config;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $pluginId, $pluginDefinition) {
    return new static(
      $configuration,
      $pluginId,
      $pluginDefinition,
      $container->get('social_graphql.twitter'),
      $container->get('config.factory')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $pluginId, $pluginDefinition, TwitterPostCollector $social_graphql_twitter, ConfigFactoryInterface $config_factory) {
    parent::__construct($configuration, $pluginId, $pluginDefinition);
    $this->twitterService = $social_graphql_twitter;
    $this->config = $config_factory->get('social_graphql.twittersettings');
  }

  /**
   * {@inheritdoc}
   */
  public function resolveValues($value, array $args, ResolveContext $context, ResolveInfo $info) {

    // Twitter information not filled.
    if (empty($this->twitterService->getSocial())) {
      throw new Error(sprintf("%s", t('No twitter instance, please verify your access configuration.')));
    }

    $posts = $this->twitterService->getPosts($args);
    if (!empty($posts->errors)) {
      foreach ($posts->errors as $error) {
        if ($error->message) {
          throw new Error(sprintf("%s", $error->message));
        }
      }
    }
    // Empty posts.
    if (empty($posts)) {
      yield   [
        'type' => 'SocialGraphQlTwitterPost',
        'post' => '',
      ];
    }
    // Fetch all posts.
    foreach ($posts as $post) {
      yield  [
        'type' => 'SocialGraphQlTwitterPost',
        'post' => $post,
      ];
    }
  }

}
