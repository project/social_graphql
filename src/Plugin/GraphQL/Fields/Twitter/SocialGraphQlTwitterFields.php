<?php

namespace Drupal\social_graphql\Plugin\GraphQL\Fields\Twitter;

use Drupal\graphql\GraphQL\Execution\ResolveContext;
use Drupal\graphql\Plugin\GraphQL\Fields\FieldPluginBase;
use GraphQL\Type\Definition\ResolveInfo;

/**
 * @GraphQLField(
 *   id = "social_graphql_twitter_fields",
 *   secure = true,
 *   deriver =
 *   "Drupal\social_graphql\Plugin\Deriver\Fields\SocialGraphQlTwitterFieldsDeriver"
 * )
 */
class SocialGraphQlTwitterFields extends FieldPluginBase {

  /**
   * {@inheritdoc}
   */
  public function resolveValues($value, array $args, ResolveContext $context, ResolveInfo $info) {

    $plugin_definition = $this->getPluginDefinition();
    if (empty($plugin_definition['field_name']) || empty($plugin_definition['social_gql_object']) || empty($value[$plugin_definition['social_gql_object']])) {
      return '';
    }
    // Get the return graphql type.
    if (!empty($info->returnType->name)) {
      $type_name = $info->returnType->name;
    }
    elseif (!empty($info->returnType->ofType->name)) {
      $type_name = $info->returnType->ofType->name;
    }
    if (empty($type_name)) {
      return '';
    }
    $data = $value[$plugin_definition['social_gql_object']];
    $field_name = $plugin_definition['field_name'];
    // Object Twitter.
    if (strpos($type_name, 'SocialGraphQlTwitter') !== FALSE) {
      $object = '';
      // Field direct.
      if (!empty($data->{$field_name})) {
        $object = $data->{$field_name};
      }
      // Inside extended entities.
      if (!empty($data->extended_entities->{$field_name}) && empty($object)) {
        $object = $data->extended_entities->{$field_name};
      }
      // Inside entities.
      if (!empty($data->entities->{$field_name}) && empty($object)) {
        $object = $data->entities->{$field_name};
      }
      if (is_array($object)) {
        foreach ($object as $obj) {
          yield [
            'type' => $type_name,
            $field_name => $obj,
          ];
        }
      }
      else {
        yield [
          'type' => $type_name,
          $field_name => $object,
        ];
      }
    }
    else {
      // Get the field value.
      if (!empty($data->{$field_name})) {
        if (is_array($data->{$field_name})) {
          foreach ($data->{$field_name} as $obj) {
            yield $obj;
          }
        }
        else {
          yield $data->{$field_name};
        }
      }
      else {
        yield '';
      }
    }
  }

}
