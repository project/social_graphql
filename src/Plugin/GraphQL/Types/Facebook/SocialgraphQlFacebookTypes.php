<?php

namespace Drupal\social_graphql\Plugin\GraphQL\Types\Facebook;

use Drupal\social_graphql\Plugin\GraphQL\Types\SocialGraphQlPostBase;

/**
 * @GraphQLType(
 *   id = "social_graphql_facebook_types",
 *   name = "social_graphql_facebook_types",
 *   deriver =
 *   "Drupal\social_graphql\Plugin\Deriver\Types\SocialGraphQlFacebookTypesDeriver"
 * )
 */
class SocialgraphQlFacebookTypes extends SocialGraphQlPostBase {


}
