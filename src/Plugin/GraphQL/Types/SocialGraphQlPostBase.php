<?php


namespace Drupal\social_graphql\Plugin\GraphQL\Types;

use Drupal\graphql\GraphQL\Execution\ResolveContext;
use Drupal\graphql\Plugin\GraphQL\Types\TypePluginBase;
use GraphQL\Type\Definition\ResolveInfo;

/**
 * Class SocialGraphQlPostBase.
 *
 * @package Drupal\social_graphql\Plugin\GraphQL\Types
 */
abstract class SocialGraphQlPostBase extends TypePluginBase {

  /**
   * {@inheritdoc}
   */
  public function applies($object, ResolveContext $context, ResolveInfo $info) {
    $definition = $this->getPluginDefinition();
    $type = isset($object['type']) && $object['type'] == $definition['name'];
    if (is_array($object)) {
      return $type && isset($object[$definition['social_gql_object']]);
    }
    return FALSE;
  }

}
