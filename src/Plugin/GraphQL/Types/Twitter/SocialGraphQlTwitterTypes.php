<?php

namespace Drupal\social_graphql\Plugin\GraphQL\Types\Twitter;

use Drupal\social_graphql\Plugin\GraphQL\Types\SocialGraphQlPostBase;

/**
 * @GraphQLType(
 *   id = "social_graphql_twitter_types",
 *   deriver =
 *   "Drupal\social_graphql\Plugin\Deriver\Types\SocialGraphQlTwitterTypesDeriver"
 * )
 */
class SocialGraphQlTwitterTypes extends SocialGraphQlPostBase {

}