<?php

namespace Drupal\social_graphql\Services\Facebook;

use Drupal\Component\Utility\UrlHelper;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\social_graphql\Services\SocialPostCollectorBase;
use Facebook\Facebook;
use GraphQL\Error\Error;
use GuzzleHttp\ClientInterface;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class FacebookPostCollector.
 *
 * @package Drupal\socialfeed
 */
class FacebookPostCollector extends SocialPostCollectorBase {

  const FB_GRAPH_URI = 'https://graph.facebook.com';

  use StringTranslationTrait;

  /**
   * Facebook application id.
   *
   * @var string
   */
  protected $appId;

  /**
   * Facebook application secret.
   *
   * @var string
   */
  protected $appSecret;


  protected $accessToken;

  /**
   * Http client.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  protected $httpClient;

  /**
   * Facebook Client.
   *
   * @var \Facebook\Facebook
   */
  private $facebook;

  /**
   * Facebook page name.
   *
   * @var string
   */
  private $pageName;

  /**
   * {@inheritdoc}
   */
  public function __construct(ConfigFactoryInterface $config_factory, LoggerChannelFactoryInterface $logger, ClientInterface $http_client) {
    parent::__construct($config_factory, $logger, 'facebook');
    $this->httpClient = $http_client;
    $this->createSocial();
  }

  /**
   * Set the Facebook client.
   *
   * @throws \Facebook\Exceptions\FacebookSDKException
   */
  public function createSocial() {
    if (!parent::createSocial()) {
      return FALSE;
    }
    $this->appId = $this->config->get('app_id');
    $this->appSecret = $this->config->get('secret_key');
    $this->accessToken = $this->config->get('access_token');
    $this->pageName = $this->config->get('page_name');
    if (empty($this->facebook)) {
      $this->facebook = new Facebook([
        'app_id' => $this->appId,
        'app_secret' => $this->appSecret,
        'default_graph_version' => 'v3.3',
      ]);
      $this->facebook->setDefaultAccessToken($this->accessToken);
    }
  }

  public function setSocial($app_id, $app_secret, $access_token) {
    $this->appId = $app_id;
    $this->appSecret = $app_secret;
    $this->accessToken = $access_token;
    $this->facebook = new Facebook([
      'app_id' => $this->appId,
      'app_secret' => $this->appSecret,
      'default_graph_version' => 'v3.3',
    ]);
    $this->facebook->setDefaultAccessToken($this->accessToken);
  }

  /**
   * {@inheritdoc}
   */
  public function getRequiredKeyAuth() {
    return [
      'app_id',
      'secret_key',
      'access_token',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getSocial() {
    return $this->facebook;
  }

  /**
   * Fetch Facebook posts from a given feed.
   *
   * @param string $page_name
   *   The name of the page to fetch results from.
   * @param int $num_posts
   *   The number of posts to return.
   *
   * @return array
   *   An array of Facebook posts.
   *
   * @throws \Facebook\Exceptions\FacebookSDKException
   * @throws \GraphQL\Error\Error
   */
  public function getPosts($page_name, $num_posts = 10) {
    $posts = [];
    // Special case for post, we must to spicify all fields.
    $post = $this->config->get('social_gql_post');
    if (empty($post['fields_map'])) {
      throw new Error(sprintf("%s", $this->t('No post mapping found please verify your configuration')));
    }
    $param['fields'] = implode(',', array_keys($post['fields_map']));
    $param['limit'] = $num_posts;
    $url = $page_name . '/feed?' . UrlHelper::buildQuery($param);
    do {
      $response = $this->facebook->get($url);
      // Ensure not caught in an infinite loop if there's no next page.
      $url = NULL;
      if ($response->getHttpStatusCode() != Response::HTTP_OK) {
        break;
      }
      $data = json_decode($response->getBody(), TRUE);
      if (empty($data['data']) && empty($data['paging']['next'])) {
        break;
      }
      $posts = array_merge($data['data'], $posts);
      $post_count = count($posts);
      if ($post_count < $num_posts && isset($data['paging']['next'])) {
        $url = $posts['paging']['next'];
      }

    } while ($post_count < $num_posts && $url != NULL);
    return array_slice($posts, 0, $num_posts);
  }

  /**
   * Extract information from the Facebook feed.
   *
   * @param string $post_types
   *   The type of posts to extract.
   * @param array $data
   *   An array of data to extract information from.
   *
   * @return array
   *   An array of posts.
   */
  protected function extractFacebookFeedData($post_types, array $data) {
    $posts = array_map(function ($post) {
      return $post;
    }, $data);

    // Filtering needed.
    if (TRUE == is_string($post_types)) {
      $posts = array_filter($posts, function ($post) use ($post_types) {
        return $post['type'] === $post_types;
      });
      return $posts;
    }
    return $posts;
  }

  /**
   * Generate the Facebook access token.
   *
   * @return string
   *   The access token.
   */
  protected function defaultAccessToken() {
    if (empty($this->config->get('access_token'))) {
      $access = $this->getAccessToken($this->appId, $this->appSecret);
      if (!empty($access->access_token)) {
        $this->config->set('access_token', $access->access_token);
      }
    }

    return $this->config->get('access_token');
  }

  /**
   * @param $app_id
   * @param $app_secret
   *
   * @return bool|mixed
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public function getPageAccessToken($page_id, $access_token) {
    $param['field'] = 'access_token';
    $param['access_token'] = $access_token;
    $param = '?' . UrlHelper::buildQuery($param);
    $request = $this->httpClient->request('GET', self::FB_GRAPH_URI . '/' . $page_id . $param);
    if (!empty($request)) {
      $content = $request->getBody()->getContents();
      if (!empty($content)) {
        return json_decode($content);
      }

    }
    return FALSE;
  }

  /**
   * Request a long live token.
   *
   * @param $app_id
   * @param $app_secret
   * @param $access_token
   *
   * @return bool|mixed
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public function longLiveToken($app_id, $app_secret, $access_token) {
    $param = [
      'grant_type' => 'fb_exchange_token',
      'client_id' => $app_id,
      'client_secret' => $app_secret,
      'fb_exchange_token' => $access_token,
    ];
    $param = '?' . UrlHelper::buildQuery($param);
    $request = $this->httpClient->request('GET', 'https://graph.facebook.com/oauth/access_token' . $param);
    if (!empty($request)) {
      $content = $request->getBody()->getContents();
      if (!empty($content)) {
        return json_decode($content);
      }

    }
    return FALSE;
  }

  /**
   * Request me/accounts.
   *
   * After request long live token, this will generate an no expire.
   *
   * @param string $access_token
   *   The access token.
   *
   * @return bool|mixed
   * @throws \Facebook\Exceptions\FacebookSDKException
   */
  public function getMeAccounts($access_token) {
    $response = $this->facebook->get('/me/accounts', $access_token);
    if ($response->getHttpStatusCode() != Response::HTTP_OK) {
      return FALSE;
    }
    $data = json_decode($response->getBody(), TRUE);
    return $data;
  }

  /**
   * @param $app_id
   * @param $app_secret
   *
   * @return bool|mixed
   * @throws \GuzzleHttp\Exception\GuzzleException
   * @todo FB connect.
   */
  public function getAccessToken($app_id, $app_secret) {
    $param = '?client_id=' . $app_id . '&client_secret=' . $app_secret . '&grant_type=client_credentials';
    $request = $this->httpClient->request('GET', 'https://graph.facebook.com/oauth/access_token' . $param);
    if (!empty($request)) {
      $content = $request->getBody()->getContents();
      if (!empty($content)) {
        return json_decode($content);
      }

    }
    return FALSE;
  }

  /**
   * Call endpoint debug token.
   *
   * @param string $access_token
   *   The access token.
   *
   * @return bool|mixed
   * @throws \Facebook\Exceptions\FacebookSDKException
   */
  public function debugToken($access_token) {
    $param['input_token'] = $access_token;
    $param = '?' . UrlHelper::buildQuery($param);
    $response = $this->facebook->get('/debug_token' . $param, $access_token);
    if ($response->getHttpStatusCode() != Response::HTTP_OK) {
      return FALSE;
    }
    $data = json_decode($response->getBody(), TRUE);
    return $data;
  }

}
