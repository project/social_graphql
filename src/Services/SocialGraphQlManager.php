<?php

namespace Drupal\social_graphql\Services;

use Drupal\Core\Extension\ModuleHandlerInterface;
use Symfony\Component\Yaml\Yaml;

/**
 * Class FacebookPostCollector.
 *
 * @package Drupal\socialfeed
 */
class SocialGraphQlManager {

  /**
   * Internal path for the mapping.
   */
  const PATH_MAPPING = '/src/Mapping';


  protected $moduleHandler;

  /**
   * {@inheritDoc}.
   */
  public function __construct(ModuleHandlerInterface $module_handler) {
    $this->moduleHandler = $module_handler;
  }

  /**
   * @param $social_network_folder
   *
   * @return array
   */
  public function getMapping($social_network_folder) {
    $module_path = $this->moduleHandler->getModule('social_graphql')
        ->getPath() . self::PATH_MAPPING . '/' . $social_network_folder;
    $files = file_scan_directory($module_path, '/(.yml)$/i', ['recurse' => 1]);
    $social_entities = [];
    // Search file with view mode.
    foreach ($files as $file) {
      $type = str_replace('.yml', '', $file->filename);
      $social_entities[$type] = Yaml::parse(file_get_contents($file->uri));
    }
    return $social_entities;
  }

}
