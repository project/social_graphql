<?php

namespace Drupal\social_graphql\Services;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;

/**
 * Class TwitterPostCollector.
 *
 * @package Drupal\social_graphql\Services
 */
abstract class SocialPostCollectorBase implements SocialPostCollectorInterface {

  protected $logger;

  protected $config;

  /**
   * {@inheritdoc}
   */
  public function __construct(ConfigFactoryInterface $configFactory, LoggerChannelFactoryInterface $logger, $social_name) {
    $this->logger = $logger->get('social_graphql_' . $social_name);
    $this->config = $configFactory->get('social_graphql.' . $social_name);
  }

  /**
   * Create the social network instance, verify if all key config are set.
   *
   * @return bool
   *   Return true if all keys required are set.
   */
  public function createSocial() {
    if (!empty($this->getSocial())) {
      return TRUE;
    }
    $key_requred = $this->getRequiredKeyAuth();
    foreach ($key_requred as $key) {
      if (empty($this->config->get($key))) {
        $this->logger->error(t("Missing config : %key.", ['%key' => $key]));
        return FALSE;
      }
    }
    return TRUE;
  }

}
