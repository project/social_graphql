<?php

namespace Drupal\social_graphql\Services;

/**
 * Interface SocialPostCollectorInterface
 *
 * @package Drupal\social_graphql\Services
 */
interface SocialPostCollectorInterface {

  /**
   * Get list of required field to authenticate service.
   *
   * @return array|false
   *   List of field.
   */
  public function getRequiredKeyAuth();

  /**
   * Create the social network instance.
   *
   * @return bool
   *   Return true if created.
   */
  public function createSocial();

  /**
   * Return the social network object library.
   *
   * @return mixed
   *   The social network object.
   */
  public function getSocial();

}
