<?php

namespace Drupal\social_graphql\Services\Twitter;

use Abraham\TwitterOAuth\TwitterOAuth;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\social_graphql\Services\SocialPostCollectorBase;

/**
 * Class TwitterPostCollector.
 *
 * @package Drupal\social_graphql\Services
 */
class TwitterPostCollector extends SocialPostCollectorBase {

  /**
   * Twitter application consumer key.
   *
   * @var string
   */
  protected $consumerKey;

  /**
   * Twitter application consumer secret.
   *
   * @var string
   */
  protected $consumerSecret;

  /**
   * Twitter application access token.
   *
   * @var string
   */
  protected $accessToken;

  /**
   * Twitter application access token secret.
   *
   * @var string
   */
  protected $accessTokenSecret;

  /**
   * Twitter OAuth client.
   *
   * @var \Abraham\TwitterOAuth\TwitterOAuth
   */
  protected $twitter;

  /**
   * {@inheritdoc}
   */
  public function __construct(ConfigFactoryInterface $config_factory, LoggerChannelFactoryInterface $logger) {
    parent::__construct($config_factory, $logger, 'twitter');
    $this->createSocial();
  }

  /**
   * Create the twitter instance.
   *
   * @return bool
   *   True if can create.
   */
  public function createSocial() {

    if (!parent::createSocial()) {
      return FALSE;
    }
    $this->consumerKey = $this->config->get('consumer_key');
    $this->consumerSecret = $this->config->get('consumer_secret');
    $this->accessToken = $this->config->get('access_token');
    $this->accessTokenSecret = $this->config->get('access_token_secret');

    $this->twitter = new TwitterOAuth(
      $this->consumerKey,
      $this->consumerSecret,
      $this->accessToken,
      $this->accessTokenSecret
    );
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function getRequiredKeyAuth() {
    return [
      'consumer_key',
      'consumer_secret',
      'access_token',
      'access_token_secret',
    ];
  }

  /**
   * Return Twitter instance.
   *
   * @return \Abraham\TwitterOAuth\TwitterOAuth
   *   The twitter object.
   */
  public function getSocial() {
    return $this->twitter;
  }

  /**
   * Retrieve Tweets from the given accounts home page.
   *
   * @see https://developer.twitter.com/en/docs/tweets/timelines/api-reference/get-statuses-user_timeline.html
   *
   * @param array $options
   *   Array of options. For more information look the twitter API doc.
   *
   * @return array|bool|object
   */
  public function getPosts(array $options = []) {
    if (empty($this->twitter)) {
      $this->logger->error(t('Tweeter instance is not initialized'));
      return FALSE;
    }
    $count = !empty($options['count']) ? $options['count'] : $this->config->get('tweets_count');
    if (empty($count) || $count < 1 || $count > 200) {
      $this->logger->error(t('Invalid count tweet value'));
      return FALSE;
    }
    $options['tweet_mode'] = 'extended';
    $options['include_entities'] = TRUE;
    return $this->twitter->get('statuses/user_timeline', $options);
  }

}
